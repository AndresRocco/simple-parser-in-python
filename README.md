# Simple Parser in Python

A simple and badly written parser implemented in python. 

Only the bare essential functionality has been implemented for a basic Compiler course at the undergrad level. It could break or malfunction with certain text files.

An example of a simple text file that could be interpreted is the following:

```
main_content
    minus 3 2
    sum 5 7
    sum 3 3 3
        div 3
```

Results would be 1, 12, 9, and 3, respectively.

Comments in code are written in spanish. Main() is huge.
