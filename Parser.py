class Root:
    def __init__(self):
        self.last_operation = None

    def get_last_op(self):
        return int(self.last_operation)

# Clase Suma
class Adition:
    def __init__(self):
        self.node1 = None
        self.node2 = None
        self.node3 = None
        self.result = None

    def add(self):
        self.result = int(self.node1) + int(self.node2)
        return int(self.result)

    def add_3(self):
        self.result = int(self.node1) + int(self.node2) + int(self.node3)
        return int(self.result)

    def get_result(self):
        return int(self.result)

# Clase Resta
class Substraction:
    def __init__(self):
        self.node1 = None
        self.node2 = None
        self.result = None

    def subtract(self):
        self.result = int(self.node1) - int(self.node2)
        return int(self.result)

    def get_result(self):
        return int(self.result)

class Div:
    def __init__(self):
        self.node1 = None
        self.node2 = None
        self.result = None

    def divide(self):
        self.result = int(self.node1) / int(self.node2)
        return int(self.result)

    def get_result(self):
        return float(self.result)

# Funcion que cuenta y regresa la cantidad de espacios en una 
# línea dada
def check_spaces(in_line):
    counter = 0
    for character in in_line:
        if character == ' ':
            counter += 1
        if character != ' ':
            break
    return counter


def main():
    operation_keywords = ("sum", "minus", "div") # Palabras clave de las posibles operaciones

    root = Root()
    suma = Adition()
    resta = Substraction()
    division = Div()

    file = open('text.txt', 'r')

    for line in file.readlines():
        for word in  line.split():
            if word in operation_keywords:
                # Si la línea tiene 4 espacios es primera jerarquía
                if check_spaces(line) == 4:
                    if word == 'sum':
                        # Si longitud de línea es 3, es suma de 2 números
                        if len(line.split()) == 3:
                            suma.node1 = line.split()[1]
                            suma.node2 = line.split()[2]
                            root.last_operation = suma.add()
                            print ("El resultado de {} es: {}".format(word, root.get_last_op()))
                        # Si el tamaño de la línea es 4, es suma de 3 números
                        if len(line.split()) == 4:
                            suma.node1 = line.split()[1]
                            suma.node2 = line.split()[2]
                            suma.node3 = line.split()[3]
                            suma.add_3()
                            root.last_operation = suma.get_result()
                            print ("El resultado de {} es: {}".format(word, root.get_last_op()))
                    if word == 'minus':
                        resta.node1 = line.split()[1]
                        resta.node2 = line.split()[2]
                        root.last_operation = resta.subtract()
                        print ("El resultado de {} es: {}".format(word, root.get_last_op()))
                # Si la línea tiene 8 espacios, es segunda jerarquía
                if check_spaces(line) == 8:
                    if word == 'sum':
                        # Si longitud de línea es 3, es suma de 2 números
                        if len(line.split()) == 3:
                            suma.node1 = line.split()[1]
                            suma.node2 = line.split()[2]
                            root.last_operation = suma.add()
                            print ("El resultado de {} es: {}".format(word, root.get_last_op()))
                        # Si el tamaño de la línea es 4, es suma de 3 números
                        if len(line.split()) == 4:
                            suma.node1 = line.split()[1]
                            suma.node2 = line.split()[2]
                            suma.node3 = line.split()[3]
                            root.last_operation = suma.add_3()
                            print ("El resultado de {} es: {}".format(word, root.get_last_op()))
                    if word == 'minus':
                        resta.node1 = line.split()[1]
                        resta.node2 = line.split()[2]
                        root.last_operation = resta.subtract()
                        print ("El resultado de {} es: {}".format(word, root.get_last_op()))
                    if word == 'div':
                        division.node1 = root.get_last_op()
                        division.node2 = line.split()[1]
                        root.last_operation = division.divide()
                        print ("El resultado de {} es: {}".format(word, root.get_last_op()))

    file.close()

if __name__ == "__main__":
    main()
